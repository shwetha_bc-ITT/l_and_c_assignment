import socket
from CustomException import CardBlockException
from CustomException import InsufficientBalanceExeption
from CustomException import InsufficientAtmBalanceExeption
from CustomException import ServerConnectException

class ATM:
    def __init__(self, user_id = 15505, pin = 9408, balance = 0):
        self.id = user_id
        self.pin = pin
        self.balance = balance
        self.daily_limit = 10001
        self.atm_balance = 100000
        
    def connect_with_server(self, server_address):
        try:
            self.sock.connect(server_address)
            #if any network discrepancy
                #raise ServerConnectException
        except socket.error:
            print("Couldnt connect with the socket-server: %s\n terminating program")
        except ServerConnectException as err:
            err.get_message()
        
    def login(self):
        attempts = 0
        try:
            while attempts<3:
                pin = int(input("Enter the 4 digit pin:\n")) 
                if pin == self.pin:
                    return True
                else:
                    attempts+=1
            raise CardBlockException
        except CardBlockException as err:
            err.get_message()
            return False
        
    def get_balance(self):
        return self.balance
    
    def validate_amount(self):
        amount = int(input("Enter the amount in multiples of 500\n"))
        if amount > self.daily_limit:
            amount = int(input("Amount exceeded daily limit. Enter amount less than 10,000\n"))
        #giving one more chance to user for entering valid amount within daily limit
        if amount < self.daily_limit:
            return True, amount
        else:
            return False, amount
    
    def withdraw_money(self, amount):
        try:
            if amount<=self.get_balance():
                self.balance -= amount
                self.atm_balance -= amount
                if self.atm_balance < 0:
                    raise InsufficientAtmBalanceExeption
                balance = account.get_balance()
                print("Collect cash!")
                print(f"Your account balance is {balance}")
            else:
                raise InsufficientBalanceExeption
        except InsufficientBalanceExeption as err:
            err.get_message()
        except InsufficientAtmBalanceExeption as err:
            err.get_message()
            
if __name__=='__main__':
    account = ATM(15505, 9408, 50000) #(user_id, pin, account_balance)
    is_complete = False
    is_valid = account.login()
    if is_valid == True:
        option = int(input("Welcome! \nSelect one of the options below: "
       "\n1. Balance Enquiry"
       "\n2. Withdraw Money"
       "\n3. Quit\n"))
        while not is_complete:
            if option==1:
                balance = account.get_balance()
                print(f"Your account balance is {balance}")
                is_complete = True
                
            elif option==2:
                is_valid_amount, amount = account.validate_amount()
                if is_valid_amount == True:
                    account.withdraw_money(amount)
                    is_complete = True
                else:
                    print("Amount exceeded daily limit. Try again later!")
                    break
                
            elif option==3:
                is_complete = True
                
            else:
                print("Invalid choice!")
                is_complete = True
                
    