class ServerConnectException(Exception):
    def __init__(self, message = "Unable to connect to the server"):
        self.message = message
        
    def get_message(self):
        print(self.message)
        
class CardBlockException(Exception):
    def __init__(self, message = "Exceeded total number of attempts. Your card has been blocked!"):
        self.message = message
        
    def get_message(self):
        print(self.message)

class InsufficientBalanceExeption(Exception):
    def __init__(self, message = "Insuffient funds! Please check your balance and try again later"):
        self.message = message        
        
    def get_message(self):
        print(self.message)
        
class InsufficientAtmBalanceExeption(Exception):
    def __init__(self, message = "Insuffient funds in the ATM machine! Try entering lesser amount"):
        self.message = message        
        
    def get_message(self):
        print(self.message)