from random import randint
import getpass 

class Wallet:
    def __init__(self, value=1000):
        self.value = value
        factory = Factory()
        factory.make_payment()
                    
    def getTotalMoney(self):
        return self.value
    
    def setTotalMoney(self, newValue):
        self.value=newValue
        
    def addMoney(self, deposit):
        self.value+=deposit
        
    def subtractMoney(self, debit):
        self.value-=debit

class Customer:
    def __init__(self, firstName='John',lastName='Warden'):
        self.firstName = firstName
        self.lastName = lastName
        
    def getFirstName(self):
        return self.firstName
    
    def getLastName(self):
        return self.lastName
    
    def getAmount(self, billAmount):
        myWallet = Wallet()
        if (myWallet.getTotalMoney() > billAmount):
            myWallet.subtractMoney(billAmount)
            return billAmount
        else:
            return 0

class Login_with_OTP:
    def __init__(self):
        self.user_info = {'phone':'9591392537'}
        self.otp = randint(1000,9999)
        self.invalid = False
        
    def get_OTP(self):
        print("Your OTP is:",self.otp)
    
    def validate_otp(self):
        while not self.invalid:
            phone = input("Enter phone number\n")
            self.get_OTP()
            OTP = int(getpass.getpass(prompt='Enter the 4 digit OTP'))
            if (phone != self.user_info['phone'])|(OTP!=self.otp):
                print("Invalid OTP\n")
            else:
                self.invalid = True    
            
class Login_with_ID:
    def __init__(self):
        self.user_info = {'user_id':'155055', 'password':'qwerty@123'}
        self.invalid = False

    def validate_credentials(self):
        while not self.invalid:
            user_id = input("Enter your user id\n")
            password = getpass.getpass(prompt='Enter you password')
            if (user_id!=self.user_info['user_id'])|(password!=self.user_info['password']):
                print("Invalid user id or password\n")
            else:
                self.invalid = True
            
class Factory(Wallet):
    def __init__(self):
        pass
    
    def getUserChoice(self):
        choice = input("Login using:\n1. User Id and Password\n2. Phone Number\n Enter choice 1 or 2\n")
        return choice
    
    def make_payment(self):
        choice = self.getUserChoice()
        if choice=='1':
            login_id = Login_with_ID()
            login_id.validate_credentials()
        elif choice=='2':
            login_otp = Login_with_OTP()
            login_otp.validate_otp()
    
class PaperBoy:
    def __init__(self):
        self.payment = self.get_payment()
        
    def get_payment(self):
        amount = float(input("Enter the bill amount: "))
        return amount
    
    def  process_payment(self):
        customer = Customer()
        paidAmount = customer.getAmount(self.payment)
        if (paidAmount==self.payment):
            print("Payment done!")
        else:
            print("Come back later and get my money!")
        
if __name__=='__main__':        
    paperboy = PaperBoy()
    paperboy.process_payment()