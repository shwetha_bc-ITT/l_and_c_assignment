import unittest
from main import Divisors 

class Test_Divisors(unittest.TestCase):
    
    def test_get_input_number(self):
        div = Divisors()
        result = div.get_input()
        self.assertEqual(result, 3)
        
    def test_get_input_symbol(self):
        div = Divisors()
        result = div.get_input()
        self.assertEqual(result, "Enter a valid integer value")
        
    def test_get_output(self):
        div = Divisors()
        #for 100,22 and 15 respectively
        self.assertEqual(div.get_output(),15)
        self.assertNotEqual(div.get_output(), 3)
        self.assertNotEqual(div.get_output(), 1)
        
    
if __name__=='__main__':
    unittest.main()