from sys import exit
import math

class Divisors:
    def __init__(self):
        self.k_values = []
        
    def get_input(self):
        try:
            #testcases = 3
            testcases = int(input("Enter the number of testcases:\n"))
            return testcases
        except ValueError:
            return "Enter a valid integer value"
            print("Terminated")
            exit(0)
            
    def get_user_input(self,testcases):
        try:
            for i in range(testcases):
                self.k_values.append(int(input(f"Enter {i+1} value:\n")))
            return self.k_values
        except ValueError:
            return "Enter a valid integer value"
            print("Terminated")
            exit(0)
            
    
            
    def count_divisors(self,n) : 
        count = 0
        for i in range(1, int(n/2) + 1) : 
            if (n % i == 0) : 
                count = count + 1
            else : 
                continue
        return count 

    def get_divisors_for_consecutive_numbers(self):
        count_list=[]
        for k in self.k_values:
            count=0
            for i in range(1,k):
                #increment count if number of divisors for 2 consecutive numbers are same
                if self.count_divisors(i) == self.count_divisors(i+1):
                    count+=1
            count_list.append(count)
        return count_list

    def get_output(self):
        testcases = self.get_input()
        if testcases>=1 and testcases<math.pow(10,6):
            self.k_values = self.get_user_input(testcases)
            count_list = self.get_divisors_for_consecutive_numbers()
            print("---------\nOutput:")    
            for i in count_list:
                print(i)
            if len(count_list)==1:
                return count_list[0]
        else:
            print("Please enter a valid number for testcases between 1 to 10^6")
            
            

if __name__=='__main__':
    div = Divisors()
    div.get_output()