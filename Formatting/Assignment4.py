import requests
import json

def get_json_object(URL):
    response = requests.get(url = URL)
    json_data = response.text
    #to retain proper json format by cropping data between {}
    json_data = json_data[json_data.find('{'):json_data.rfind('}')+1]
    json_object = json.loads(json_data)
    return json_object

def get_url_parameters(total_posts):
    total_posts = total_posts.split("-")
    start, end = int(total_posts[0]) - 1, int(total_posts[1])
    url_parameters = {'type':'photo', 'num':end, 'start':start}
    return url_parameters

def get_url(blog_name,total_posts):
    url_parameters = get_url_parameters(total_posts)
    URL = 'https://'+ blog_name +'.tumblr.com/api/read/json'
    response = requests.get(url = URL, params = url_parameters)
    return response.url

def print_image_urls(json_object, total_posts):
    posts = json_object['posts']
    total_posts = total_posts.split("-")
    min_range=int(total_posts[0]) - 1
    max_range=int(total_posts[1])
    for i in range(min_range,max_range):
        if (len(posts[i]['photos']))!=0:
            print(i + 1,end=' ')
            for j in range(len(posts[i]['photos'])):
                print(posts[i]['photos'][j]['photo-url-1280'],sep=' ')
        else:
            print(i + 1,end=' ')
            print(posts[i]['photo-url-1280'],' ')

def print_basic_info(json_object):
    print("\ntitle: ", json_object['tumblelog']['title'])
    print("name: ", json_object['tumblelog']['name'])
    print("description: ", json_object['tumblelog']['description'])
    print("no of post: ", json_object['posts-total'])
    print("\nURL of images in the given post range:")

#Tumblr API v1 can return a maximum of 50 posts only    
def check_range(total_posts):
    total_posts = total_posts.split("-")
    if int(total_posts[1]) > 50:
        raise ValueError

def main():
    has_error = True
    while has_error:
        try:
            blog_name = input("Enter the blog name: ")
            total_posts = input("Enter the range: ")
            check_range(total_posts)
            url_parameters = get_url_parameters(total_posts)
            complete_URL = get_url(blog_name,total_posts)
            json_object = get_json_object(complete_URL)
            print_basic_info(json_object)
            print_image_urls(json_object, total_posts)
            has_error = False
        except (requests.exceptions.ConnectionError, json.JSONDecodeError):
            print("Page Not Found!")
            has_error = True
        except IndexError:
            print("Enter range in (value1 - value2) format!")
            has_error = True
        except ValueError:
            print("Please enter range within 50!")
            has_error = True

if __name__ == '__main__':
    main()